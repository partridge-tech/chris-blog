---
layout: page
title: About Me
permalink: /about/
sitemap:
  priority: 0.7
  freq: 'monthly'
---

Howdy - again! I'm Chris, a ~~basement-dweller~~ ~~eboy~~ cybersecurity fella on the internet. At the moment, I work with businesses to help build or expand security practices, particularly around software engineering.

I try to be a jack-of-all-trades in my work to develop a more holistic view of the cybersecurity landscape. Despite the [InfoSec Bingo](https://github.com/swagitda/infosec-buzzword-bingo)-ness, much of the work I do revolves around security visibility and accessibility, which I stand by as core tenets of effective software security. You can find some examples of my personal technical research [here](/research/), and some other work that I'm doing with some friends under a cybersecurity research entity, [Machines Never Sleep, LLC](https://mns.llc).

I also play a smidge of D&D and love cooking for myself & others. Maybe I'll start a food blog too. Anything is possible!

## Contact

You can send me an email at [tweedge-public@partridge.tech](mailto:tweedge-public@partridge.tech), connect with me on [LinkedIn](https://www.linkedin.com/in/tweedge/), or follow me on Twitter [@_tweedge](https://twitter.com/_tweedge) for more shenanigans!

## What is "tweedge"?

The moniker I use came from a close friend. Chris "Partridge" became "Partweedge" - the "ee" sound mostly from howling it in laughter - and then eventually just "'tweedge." It stuck for years, and I'm happy to have it be my handle these days.
